﻿using System;
using System.Diagnostics;

static class FLAGS {
    public const string DEBUG = "DEBUG";
}

static class Asr {
#line hidden
    [Conditional(FLAGS.DEBUG)]
    public static void IsTrue(bool expr, string msg = null) {
        if (expr) return;
        Fail(msg, msg);
    }


    [Conditional(FLAGS.DEBUG)]
    public static void IsFalse(bool expr, string msg = null) {
        if (!expr) return;
        Fail(msg);
    }

    [Conditional(FLAGS.DEBUG)]
    public static void Fail(string message, string userMessage = null) {
        if (userMessage != null)
            message = userMessage + '\n' + message;

        throw new AssertException(message);
    }

#line default

    class AssertException : Exception {
        public AssertException(string msg) : base(msg) { }
    }
}