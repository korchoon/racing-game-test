﻿using System;
using System.Collections;
using UnityEngine;

class Car : MonoBehaviour {
    public CarView View;
    public Transform ViewPivot;
    public CarConfig OnTrackConfig;
    public CarConfig OffTrackConfig;

    public Transform GroundRaycastPivot;
    public float OnGroundDist;
    public Rigidbody PhysRigidbody;

    // for visuals
    [NonSerialized] public float SpeedNorm;
    [NonSerialized] public bool BoostTriggered;
    public bool OnGround { get; private set; }

    // state
    public float Speed { get; private set; } // not a real speed actually
    public bool IsBoosting { get; private set; }

    public float RotateTarget { get; private set; }
    float _rotation;
    Vector3 _physViewDelta;
    bool _onTrackPrev;


    void OnEnable() {

        PhysRigidbody.transform.SetParent(null, true);
        Asr.IsTrue(0 < OnGroundDist);

        var physLs = new Pose(PhysRigidbody.transform.localPosition, PhysRigidbody.transform.localRotation);
        var physWs = physLs.GetTransformedBy(transform);
        PhysRigidbody.transform.SetPose(physWs);

        _physViewDelta = transform.position - PhysRigidbody.transform.position;
        // todo
        // CheckGround(out _, out _, out _, out var onGround);
        // Asr.IsTrue(onGround);
    }


    void FixedUpdate() {
        CheckGround(out var hitGround, out var onTrack, out var onGround);
        OnGround = onGround;
        var goneOffTrack = _onTrackPrev && !onTrack;
        _onTrackPrev = onTrack;

        var carConfig = onTrack ? OnTrackConfig : OffTrackConfig;
        var _desiredSpeed = IsBoosting ? carConfig.MaxSpeed + carConfig.BoostDeltaSpeed : carConfig.MaxSpeed;

        // todo replay
        int inputSteer = 0;
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            inputSteer = -1;
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            inputSteer = 1;

        if (OnGround) {
            if (inputSteer != 0)
                _desiredSpeed = Mathf.Max(carConfig.BreakTargetSpeed, 0f);
            _rotation = carConfig.Steering * inputSteer;
        }

        var dt = Time.fixedDeltaTime;
        if (goneOffTrack)
            Speed = _desiredSpeed;
        else
            Speed = Mathf.SmoothStep(Speed, _desiredSpeed, dt * carConfig.Acceleration);

        SpeedNorm = Mathf.InverseLerp(0f, carConfig.MaxSpeed, Speed);
        SpeedNorm = Mathf.Clamp01(SpeedNorm);

        RotateTarget = Mathf.Lerp(RotateTarget, _rotation, dt * 4f);

        ViewPivot.up = Vector3.Lerp(ViewPivot.up, hitGround.normal, dt * 8f);
        ViewPivot.Rotate(0f, transform.eulerAngles.y, 0f);

        if (OnGround)
            PhysRigidbody.AddForce(transform.forward * Speed, ForceMode.Acceleration);
        else
            PhysRigidbody.AddForce(transform.forward * (Speed * 0.1f), ForceMode.Acceleration);

        if (OnGround) {
            var velocityLs = transform.InverseTransformVector(PhysRigidbody.velocity);
            velocityLs.x *= carConfig.Drift;
            PhysRigidbody.velocity = transform.TransformVector(velocityLs);
        }

        transform.position = PhysRigidbody.position + _physViewDelta;
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(0, transform.eulerAngles.y + RotateTarget, 0f)), Time.deltaTime * 2f);
    }

    void CheckGround(out RaycastHit hitGround, out bool onTrack, out bool onGround) {
        var origin = GroundRaycastPivot.position;
        onGround = Physics.Raycast(origin, Vector3.down, out hitGround, OnGroundDist);
        if (onGround)
            onTrack = hitGround.collider.gameObject.layer == LAYERS.Track;
        else
            onTrack = false;
        if (!onGround)
            Physics.Raycast(origin, Vector3.down, out hitGround, OnGroundDist + 0.4f);
    }

    // todo move inside car phys loop 
    public void Boost(float sec) {
        if (IsBoosting) return;
        StartCoroutine(Inner());

        IEnumerator Inner() {
            BoostTriggered = true;
            IsBoosting = true;
            yield return new WaitForSeconds(sec);
            IsBoosting = false;
        }
    }
}