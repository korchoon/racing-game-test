﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

class GameUi : MonoBehaviour {
    [Header("Start")] public GameObject StartRoot;
    public Button StartBtn;

    [Header("Menu")] public GameObject GameMenuRoot;
    public Button MenuBtn;

    public ResultsUi ResultsUi;

    [Header("Ingame")] public GameObject GameHudRoot;
    public TMP_Text Timer;
    public TMP_Text Speed;

    public Image AvailableStars;
    public Sprite[] Stars = new Sprite[4]; // 3,2,1,0 stars
}