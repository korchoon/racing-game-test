﻿using UnityEngine;

class Level : MonoBehaviour {
    // todo move there, update each car manually from carManager
    // public CarConfig OnTrack;
    // public CarConfig OffTrack; 

    public int LevelId;
    public Transform Start;
    public Transform CamPivot;
    public int Star3Sec;
    public int Star2Sec;
    public int Star1Sec;
    [Tooltip("Order matters")]
    public Checkpoint[] Checkpoints;
}