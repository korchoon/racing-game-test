﻿using UnityEngine;

static class LAYERS {
    public static int PlayerCar { get; } = GetLayer(nameof(PlayerCar));
    public static int GhostCar { get; } = GetLayer(nameof(GhostCar));
    public static int Track { get; } = GetLayer(nameof(Track));
    public static int Obstacle { get; } = GetLayer(nameof(Obstacle));

    static int GetLayer(string s) {
        var res = LayerMask.NameToLayer(s);
        Asr.IsTrue(res >= 0, $"No such layer: '{s}'");
        return res;
    }
}