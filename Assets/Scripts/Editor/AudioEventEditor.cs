﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(AudioEvent), true)]
class AudioEventEditor : Editor {
    [SerializeField] private AudioSource _previewer;

    public void OnEnable() {
        _previewer = EditorUtility.CreateGameObjectWithHideFlags("Audio preview", HideFlags.HideAndDontSave, typeof(AudioSource)).GetComponent<AudioSource>();
    }

    public void OnDisable() {
        DestroyImmediate(_previewer.gameObject);
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        if (GUILayout.Button("Preview"))
            ((AudioEvent) target).PlayOn(_previewer);
    }
}