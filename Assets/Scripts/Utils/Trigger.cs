﻿using UnityEngine.Events;

/// <summary>
/// Multi-purpose trigger, mainly for tracking Btn, EventTrigger & unsubscribe 
/// </summary>
class Trigger {
    public bool Triggered;

    public void SubscribeOnce(UnityEvent evt) {
        Triggered = false;
        evt.AddListener(Call);

        void Call() {
            Triggered = true;
            evt.RemoveListener(Call);
        }
    }

    public void Subscribe(UnityEvent evt, out UnityAction unityAction) {
        Triggered = false;
        unityAction = new UnityAction(Call);
        evt.AddListener(unityAction);

        void Call() {
            Triggered = true;
        }
    }
}