﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

static class Utils {
    public static Pose GetPose(this Transform t) {
        return new Pose(t.position, t.rotation);
    }

    public static void SetPose(this Transform t, Pose pose) {
        t.SetPositionAndRotation(pose.position, pose.rotation);
    }

    /// <summary>
    /// Prevents undesired OnEnable call during Instantiate. Allows to pass & execute initialization before OnEnable,Awake,Start happens 
    /// </summary>
    /// <param name="prefab"></param>
    /// <param name="preinit"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T InstantiatePreInit<T>(this T prefab, Action<T> preinit) where T : Component {
        var item = Object.Instantiate(prefab, Disabled);
        preinit.Invoke(item);
        if (item.transform.parent == Disabled)
            item.transform.SetParent(null, true);
        return item;
    }

    static Transform _disabled;

    static Transform Disabled {
        get {
            if (_disabled) return _disabled;
            _disabled = new GameObject("DISABLED") {
                // hideFlags = HideFlags.HideAndDontSave
            }.transform;
            _disabled.gameObject.SetActive(false);
            Object.DontDestroyOnLoad(_disabled);
            return _disabled;
        }
    }
}