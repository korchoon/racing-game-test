﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

class ResultsUi : MonoBehaviour {
    public Button RestartBtn;
    public Button[] LevelBtns;
    public Button CloseBtn;

    public Image[] StarsImg;
    public Sprite[] StarActiveInactive;

    public Text BestTime;
    public GameObject BestTimeRoot;
    public Text YourTime;

    public Image[] Levels;
    [FormerlySerializedAs("Stars")] public Sprite[] LevelStars; // 0,1,2,3

    public GameObject StarsRoot;

    void Init(Data arg) {
        Asr.IsTrue(arg.Valid());

        CloseBtn.gameObject.SetActive(arg.CloseActive);
        RestartBtn.gameObject.SetActive(arg.RestartActive);
        YourTime.transform.parent.gameObject.SetActive(arg.ScoreActive);
        StarsRoot.SetActive(arg.StarsActive);

        for (int i = 0; i < StarsImg.Length; i++) {
            var has = i < arg.Stars;
            var id = has ? 1 : 0;
            StarsImg[i].sprite = StarActiveInactive[id];
        }

        BestTimeRoot.SetActive(arg.HasBest);
        if (arg.HasBest) BestTime.text = FromElapsed(arg.Best);

        YourTime.text = FromElapsed(arg.Elapsed);

        for (int i = 0; i < 3; i++) {
            var spriteId = arg.StarsPerLevel[i];
            Levels[i].sprite = LevelStars[spriteId];
        }

        string FromElapsed(float elapsed) {
            var timeSpan = TimeSpan.FromSeconds(elapsed);
            return $"{timeSpan.Minutes:00}:{timeSpan.Seconds:00}:{timeSpan.Milliseconds:000}";
        }
    }

    public IEnumerator Flow(Data arg, Result result) {
        gameObject.SetActive(true);
        try {
            Init(arg);

            var gotoLevel = new[] {new Trigger(), new Trigger(), new Trigger(),};
            for (int i = 0; i < gotoLevel.Length; i++)
                gotoLevel[i].SubscribeOnce(LevelBtns[i].onClick);

            var restart = new Trigger();
            restart.SubscribeOnce(RestartBtn.onClick);

            var close = new Trigger();
            close.SubscribeOnce(CloseBtn.onClick);

            while (true) {
                if (close.Triggered) {
                    result.IsClose = true;
                    yield break;
                }

                if (restart.Triggered) {
                    result.IsRestart = true;
                    yield break;
                }

                for (int i = 0; i < gotoLevel.Length; i++) {
                    if (gotoLevel[i].Triggered) {
                        result.IsLevel = true;
                        result.LevelId = i;
                        yield break;
                    }
                }

                yield return null;
            }
        }
        finally {
            gameObject.SetActive(false);
        }
    }

#if test_ui
    [SerializeField] Data _test;

    [ContextMenu("Test")]
    void Test() {
        Init(_test);
    }
#endif

    [Serializable]
    public struct Data {
        public bool CloseActive;
        public bool RestartActive;
        public bool ScoreActive;
        public bool StarsActive;

        public int Stars;
        public float Elapsed;
        public bool HasBest;
        public float Best;
        public int[] StarsPerLevel;

        public bool Valid() {
            if (Stars > 3) return false;
            if (StarsPerLevel == null || StarsPerLevel.Length != 3) return false;
            foreach (var s in StarsPerLevel) {
                if (s > 3 || s < 0) return false;
            }

            return true;
        }
    }

    public class Result {
        public bool IsLevel;
        public int LevelId;
        public bool IsRestart;
        public bool IsClose;
    }
}