﻿using System.IO;
using UnityEditor;
using UnityEngine;

static class PersistenceUtils {
    static string Path => $"{Application.persistentDataPath}/RacingVR.json";

    public static GameState Load() {
        if (!File.Exists(Path)) {
            Debug.Log("No state found, creating new");
            return SaveDefault();
        }

        var json = File.ReadAllText(Path);
        if (string.IsNullOrEmpty(json)) {
            Debug.LogError("Corrupt file, overwriting with default");
            return SaveDefault();
        }

        var loaded = JsonUtility.FromJson<GameState>(json);
        if (loaded.Valid())
            return loaded;
        
        Debug.LogError("Invalid state, overwriting with default");
        return SaveDefault();

        //-
        GameState SaveDefault() {
            loaded = GameState.Default();
            Save(loaded);
            return loaded;
        }
    }

    public static void Save(in GameState state) {
        state.Version = GameState.CurrentVersion;
        var json = JsonUtility.ToJson(state);
        File.WriteAllText(Path, json);
    }

#if UNITY_EDITOR
         [MenuItem("Tools/Open State")]
    public static void OpenStateFolder() {
        Application.OpenURL(Path);
    }
#endif
}