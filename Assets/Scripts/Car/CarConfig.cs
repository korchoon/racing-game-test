﻿using UnityEngine;

[CreateAssetMenu]
class CarConfig :ScriptableObject{
    [Range(1f, 12f)] public float Acceleration = 4f;
    [Range(5f, 80f)] public float MaxSpeed = 30f;
    [Range(1f, 80f)] public float BreakTargetSpeed = 15f;
    [Range(0f, 50f)] public float BoostDeltaSpeed = 30f;
    [Range(5f, 160f)] public float Steering = 40f;
    [Range(0f, 1f)] public float Drift = 0.5f;
}