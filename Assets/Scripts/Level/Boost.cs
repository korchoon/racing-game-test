﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
class Boost : MonoBehaviour {
    public Collider Collider;
    public float Duration = 1f;

    void Reset() {
        Collider = GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider other) {
        if (!other.gameObject.TryGetComponent(out CarPhysics carPhysics)) return;
        carPhysics.Car.Boost(Duration);
    }
}