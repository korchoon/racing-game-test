﻿using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

public class BatchCar : MonoBehaviour {
    public Renderer[] Renderers;

    void Start() {
        StaticBatchingUtility.Combine(Renderers.Select(r => r.gameObject).ToArray(), gameObject);
    }
}