﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider))]
class Checkpoint : MonoBehaviour {
    public Collider Collider;
    public Image Image;
    public Color EnteredColor;

    public bool Entered;
    Color _initColor;

    void Reset() {
        Collider = GetComponent<Collider>();
    }

    void Awake() {
        _initColor = Image.color;
    }

    void OnEnable() {
        Image.color = _initColor;
        Entered = false;
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.layer != LAYERS.PlayerCar) return;
        Entered = true;
    }
}