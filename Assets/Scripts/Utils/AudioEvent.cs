﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Audio Event")]
public class AudioEvent : ScriptableObject {
    public AudioClip[] clips;
    public RangedFloat Volume;
    [MinMaxRange(0, 2)] public RangedFloat Pitch;

    public void PlayOn(AudioSource source) {
        if (clips.Length == 0) {
            Debug.LogWarning($"No AudioClips for {name}");
            return;
        }

        source.clip = clips[Random.Range(0, clips.Length)];
        source.volume = Random.Range(Volume.minValue, Volume.maxValue);
        source.pitch = Random.Range(Pitch.minValue, Pitch.maxValue);
        source.Play();
    }
}

[Serializable]
public struct RangedFloat {
    public float minValue;
    public float maxValue;
}

public class MinMaxRangeAttribute : Attribute {
    public MinMaxRangeAttribute(float min, float max) {
        Min = min;
        Max = max;
    }

    public float Min { get; }
    public float Max { get; }
}