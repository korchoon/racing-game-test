﻿using System.Collections;
using UnityEngine;
using UnityEngine.Serialization;

class CarView : MonoBehaviour {
    public Transform[] Wheels; // Front-Right, Front-Left, BL, BR
    
    public float[] WheelRadius = {.33f, 0.33f, 0.34f, 0.34f};
    public GameObject BoostRoot;
    [Header("Skidmarks")] public float SkidAngleThreshold = 5f;
    public TrailRenderer[] TrailRenderers;

    [Header("Audio")] public AudioSource EngineAudioSource;
    public AudioSource EffectAudioSource;
    public AudioClip Engine;
    public AudioEvent Boost;

    public IEnumerator ObserveCar(Car car, IScope scope) {
        EngineAudioSource.clip = Engine;
        EngineAudioSource.Play();

        var boost = StartCoroutine(ObserveBoost());
        scope.Subscribe(() => StopCoroutine(boost));

        while (true) {
            EngineAudioSource.pitch = car.SpeedNorm;
            if (car.BoostTriggered) {
                car.BoostTriggered = default;
            }

            // skidmarks
            var drifting = false;
            if (car.OnGround) {
                if (Vector3.Angle(car.PhysRigidbody.velocity, car.transform.forward) > SkidAngleThreshold)
                    drifting = true;
            }

            for (int i = 0; i < TrailRenderers.Length; i++) {
                TrailRenderers[i].emitting = drifting;
            }

            // wheels
            var realSpeed = Vector3.Dot(transform.forward, car.PhysRigidbody.velocity);
            float posDelta = realSpeed * Time.deltaTime;
            for (int i = 0; i < Wheels.Length; i++) {
                var degDelta = posDelta / WheelRadius[i] * Mathf.Rad2Deg;
                Wheels[i].Rotate(degDelta, 0f, 0f);
            }

            yield return null;
        }

        IEnumerator ObserveBoost() {
            while (true) {
                car.View.BoostRoot.SetActive(false);
                while (!car.IsBoosting) yield return null;

                Boost.PlayOn(EffectAudioSource);
                car.View.BoostRoot.SetActive(true);

                while (car.IsBoosting) yield return null;
            }
        }
    }
}