using System;
using System.Collections.Generic;

/// <summary>
/// "Dispose event". Allows to subscribe to IDisposable. 
/// Subscribers called in reverse order, it allows: cascade disposal, undo behaviours
/// </summary>
public interface IScope {
    bool Disposing { get; }

    /// <summary>
    /// Subscribe to Dispose "event". Stored in stack, will be disposed in reverse order
    /// </summary>
    /// <param name="dispose"></param>
    void Subscribe(Action dispose);

    /// <summary>
    /// Unsubscribe to prevent
    /// </summary>
    /// <param name="dispose"></param>
    void Unsubscribe(Action dispose);
}

public static class Scope {
    public static IDisposable New(out IScope scope) {
        var subject = new ScopeStack();
        scope = subject;
        return subject;
    }

    /// <summary>
    /// Make scope which get disposed with parent but could be disposed separately. 
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="child"></param>
    /// <returns></returns>
    public static IDisposable SubScope(this IScope parent, out IScope child) {
        var dispose = Scope.New(out child);
        Action disposal = dispose.Dispose;
        child.Subscribe(_Remove);
        parent.Subscribe(disposal);
        return dispose;

        //-
        void _Remove() {
            if (!parent.Disposing)
                parent.Unsubscribe(disposal);
        }
    }
}

class ScopeStack : IDisposable, IScope {
    public bool Disposing { get; private set; }

    bool _disposed;
    LinkedList<Action> _stack;

    public ScopeStack() {
        _disposed = false;
        Disposing = false;
        _stack = new LinkedList<Action>();
    }

    public void Dispose() {
        if (_disposed) return;
        if (Disposing) return; // Self-looping dispose call - probably should assert

        Disposing = true;
        Action cur = null;
        while (TryDequeue(ref cur)) {
            Asr.IsFalse(_disposed);
            cur.Invoke();
        }

        _stack.Clear();
        _stack = null;
        _disposed = true;
    }

    bool TryDequeue(ref Action value) {
        if (_stack.Count == 0) return false;

        value = _stack.Last.Value;
        _stack.RemoveLast();
        return true;
    }


    public void Subscribe(Action dispose) {
        // call immediately  
        if (_disposed || Disposing) {
            dispose.Invoke();
            return;
        }

        _stack.AddLast(dispose);
    }

    public void Unsubscribe(Action dispose) {
        if (_disposed || Disposing) {
            Asr.Fail("Cannot unsubscribe during or after disposal");
            return;
        }

        var any = _stack.Remove(dispose);
        Asr.IsTrue(any, "Delegate not found: make sure it's the same which was passed to Subscribe");
    }
}