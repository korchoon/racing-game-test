﻿using System.Collections.Generic;

class Replay {
    public Queue<int> Tick;
    public Queue<int> Input;

    // write
    int _writeCounter;
    int _lastInput = -2;

    public void Report(int input) {
        _writeCounter += 1;
        if (_lastInput == input)
            return;
        _lastInput = input;
        Tick.Enqueue(_writeCounter);
        Input.Enqueue(_lastInput);
    }

    // read
    int _readCounter;
    int _lastRead;

    public bool TryRead(out int res) {
        _readCounter += 1;
        if (_lastRead > _readCounter) {
            res = _lastInput;
            return true;
        }

        if (Input.Count == 0) {
            res = default;
            return false;
        }

        _lastInput = Input.Dequeue();
        _lastRead = Tick.Dequeue();
        res = _lastInput;
        return true;
    }
}