﻿using System;

[Serializable]
struct LevelState {
    public float BestTime;
    public int Stars;
    // public byte[] Replay;
    public bool IsNull() => BestTime == default ;
}

[Serializable]
class GameState {
    public const int CurrentVersion = 2;
    public const int LevelCount = 3;

    public int Version;
    public LevelState[] Levels;

    public static GameState Default() => new GameState() {
        Levels = new LevelState[LevelCount],
    };


    public bool Valid() {
        return Version == CurrentVersion && LevelCount == Levels.Length;
    }
}