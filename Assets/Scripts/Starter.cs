﻿using System;
using System.Collections;
using System.Linq;
using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;

class Starter : MonoBehaviour {
    public GameObject DontDestroyRoot;
    public GameUi Ui;
    public Car CarPrefab;
    public CinemachineVirtualCamera VirtualCamera;

    IDisposable _disposeScope;
    bool _quitting;
    Car _car;
    Level _level;

    LevelState _levelState;
    GameState _gameState;

    bool _hasPrevChoice;
    ResultsUi.Result _prevChoice;

    void Awake() {
        DontDestroyOnLoad(DontDestroyRoot);
    }

    void OnEnable() {
        _gameState = PersistenceUtils.Load();
        _disposeScope = Scope.New(out var scope);

        var flow = StartCoroutine(Flow(scope));
        scope.Subscribe(() => StopCoroutine(flow));
    }

    void OnApplicationQuit() {
        _quitting = true;
    }

    void OnDisable() {
        if (_quitting) return;
        _disposeScope.Dispose();
    }

    IEnumerator Flow(IScope scope) {
        ResultsUi.Result menuRes;
        if (_hasPrevChoice)
            menuRes = _prevChoice;
        else {
            menuRes = new ResultsUi.Result();
            var menuArg = new ResultsUi.Data() {
                RestartActive = false,
                ScoreActive = false,
                CloseActive = false,
                HasBest = false,
                StarsPerLevel = _gameState.Levels.Select(s => s.Stars).ToArray(),
            };

            yield return Ui.ResultsUi.Flow(menuArg, menuRes);
            Asr.IsTrue(menuRes.IsLevel);
        }

        if (menuRes.IsLevel) {
            var op = SceneManager.LoadSceneAsync(menuRes.LevelId + 1, new LoadSceneParameters(LoadSceneMode.Single));
            op.allowSceneActivation = true;
            while (!op.isDone)
                yield return null;

            _level = FindObjectOfType<Level>();
            if (_level)
                yield return null;
        }

        _levelState = _gameState.Levels[_level.LevelId];
        Ui.StartRoot.SetActive(true);
        _hasPrevChoice = true;

        // hack
        VirtualCamera.gameObject.SetActive(false);
        VirtualCamera.transform.SetPose(_level.CamPivot.GetPose());
        VirtualCamera.gameObject.SetActive(true);

        var startTrigger = new Trigger();
        startTrigger.SubscribeOnce(Ui.StartBtn.onClick);
        while (!startTrigger.Triggered)
            yield return null;
        Ui.StartRoot.SetActive(false);

        _car = Instantiate(CarPrefab);

        var speedTime = StartCoroutine(ObserveSpeed());
        var checkpoints = StartCoroutine(ObserveCheckpoints());
        scope.Subscribe(() => {
            Destroy(_car.gameObject);
            Destroy(_car.PhysRigidbody.gameObject);
            StopCoroutine(speedTime);
            StopCoroutine(checkpoints);
        });
        _car.PhysRigidbody.isKinematic = true;
        _car.gameObject.SetActive(false);
        _car.transform.SetPose(_level.Start.GetPose());
        _car.PhysRigidbody.gameObject.layer = LAYERS.PlayerCar;

        VirtualCamera.Follow = _car.transform;
        VirtualCamera.LookAt = _car.transform;

        using (scope.SubScope(out var levelScope)) {
            Ui.GameHudRoot.SetActive(true);
            Ui.GameMenuRoot.SetActive(true);
            _car.gameObject.SetActive(true);
            _car.PhysRigidbody.isKinematic = false;
            var visuals = StartCoroutine(_car.View.ObserveCar(_car, levelScope));
            var pauseResult = new ResultsUi.Result();
            var pause = StartCoroutine(ObservePause(levelScope, pauseResult));

            var levelDone = new Trigger();
            var timerRules = StartCoroutine(ObserveTimers(levelScope, levelDone));

            levelScope.Subscribe(() => {
                Ui.GameHudRoot.SetActive(false);
                Ui.GameMenuRoot.SetActive(false);
                _car.gameObject.SetActive(false);
                _car.PhysRigidbody.isKinematic = true;
                StopCoroutine(visuals);
                StopCoroutine(pause);
                StopCoroutine(timerRules);

                // hack to reset colors on restart
                _level.gameObject.SetActive(false);
                _level.gameObject.SetActive(true);
            });

            while (true) {
                if (levelDone.Triggered)
                    break;

                if (pauseResult.IsRestart || pauseResult.IsLevel) {
                    Finalize(pauseResult);
                    yield break;
                }

                yield return null;
            }

            while (!levelDone.Triggered)
                yield return null;
        }


        var arg = new ResultsUi.Data() {
            RestartActive = true,
            ScoreActive = true,
            StarsActive = true,
            CloseActive = false,
            Best = _levelState.BestTime,
            HasBest = _levelState.BestTime != default,
            Elapsed = _elapsed,
            Stars = _stars,
            StarsPerLevel = _gameState.Levels.Select(s => s.Stars).ToArray(),
        };
        var result = new ResultsUi.Result();
        yield return Ui.ResultsUi.Flow(arg, result);
        if (result.IsRestart || result.IsLevel)
            Finalize(result);
    }

    void Finalize(ResultsUi.Result result) {
        if (result.IsLevel) {
            if (result.LevelId == _level.LevelId) {
                result.IsLevel = false;
                result.IsRestart = true;
            }
        }

        _prevChoice = result;
        _hasPrevChoice = true;
        OnDisable();
        OnEnable();
    }

    IEnumerator ObserveTimers(IScope scope, Trigger levelDone) {
        var startTime = Time.time;
        var timers = new[] {_level.Star3Sec, _level.Star2Sec, _level.Star1Sec};

        using (scope.SubScope(out var gameScope)) {
            var timer = StartCoroutine(Timer());
            gameScope.Subscribe(() => StopCoroutine(timer));

            for (_stars = 0; _stars < timers.Length ; _stars++) {
                Ui.AvailableStars.sprite = Ui.Stars[_stars];
                var cur = timers[_stars];
                // todo blink


                while (true) {
                    if (Elapsed() > cur)
                        break;

                    if (_allCheckpointsPassed)
                        goto PASSED; // to break outer loop inside an inner loop

                    yield return null;
                }
            }

            Ui.AvailableStars.sprite = Ui.Stars[_stars];

            while (!_allCheckpointsPassed)
                yield return null;
        }

        PASSED:

        _elapsed = Elapsed();
        levelDone.Triggered = true;
        if (!_levelState.IsNull() && Elapsed() >= _levelState.BestTime)
            yield break;

        _levelState.BestTime = Elapsed();
        _gameState.Levels[_level.LevelId] = _levelState;
        PersistenceUtils.Save(_gameState);
        Debug.Log("Score saved");

        //-
        float Elapsed() {
            return Time.time - startTime;
        }

        IEnumerator Timer() {
            var tick = new WaitForSeconds(1f);
            while (true) {
                var span = TimeSpan.FromSeconds(Elapsed());
                Ui.Timer.text = $"{span.Minutes}:{span.Seconds:00}";
                yield return tick;
            }
        }
    }

    bool _allCheckpointsPassed;
    float _elapsed;
    int _stars;

    IEnumerator ObserveCheckpoints() {
        _allCheckpointsPassed = false;
        var startTime = Time.time;
        foreach (var checkpoint in _level.Checkpoints) {
            checkpoint.Entered = false;
            while (!checkpoint.Entered)
                yield return null;

            checkpoint.Image.color = checkpoint.EnteredColor;
        }

        _allCheckpointsPassed = true;

        var elapsed = Time.time - startTime;
        if (!_levelState.IsNull() && elapsed >= _levelState.BestTime)
            yield break;

        _levelState.BestTime = elapsed;
        _gameState.Levels[_level.LevelId] = _levelState;
        PersistenceUtils.Save(_gameState);
        Debug.Log("Score saved");
    }

    IEnumerator ObserveSpeed() {
        var tick = new WaitForSeconds(0.1f);
        while (true) {
            // todo cache number strings, stringBuilder
            var speedText = _car.Speed.ToString("00.0");
            Ui.Speed.text = $"{speedText} km/h";
            yield return tick;
        }
    }

    IEnumerator ObservePause(IScope scope, ResultsUi.Result res) {
        var pauseBtn = new Trigger();
        pauseBtn.Subscribe(Ui.MenuBtn.onClick, out var callback);
        scope.Subscribe(() => Ui.MenuBtn.onClick.RemoveListener(callback));
        Time.timeScale = 1f;
        AudioListener.pause = false;

        while (true) {
            while (!pauseBtn.Triggered)
                yield return null;
            pauseBtn.Triggered = false;

            using (scope.SubScope(out var pauseScope)) {
                Time.timeScale = 0f;
                AudioListener.pause = true;
                Ui.GameHudRoot.SetActive(false);
                Ui.MenuBtn.gameObject.SetActive(false);

                pauseScope.Subscribe(() => {
                    Ui.MenuBtn.gameObject.SetActive(true);
                    Time.timeScale = 1f;
                    AudioListener.pause = false;
                    Ui.GameHudRoot.SetActive(true);
                });

                var arg = new ResultsUi.Data() {
                    ScoreActive = false,
                    StarsActive = false,
                    RestartActive = true,
                    CloseActive = true,
                    Best = _levelState.BestTime,
                    HasBest = _levelState.BestTime != default,
                    Elapsed = _elapsed,
                    Stars = _stars,
                    StarsPerLevel = _gameState.Levels.Select(s => s.Stars).ToArray(),
                };
                var local = new ResultsUi.Result();
                yield return Ui.ResultsUi.Flow(arg, local);

                if (local.IsRestart) {
                    res.IsRestart = true;
                    yield break;
                }

                if (local.IsLevel) {
                    res.IsLevel = true;
                    res.LevelId = local.LevelId;
                    yield break;
                }

                Asr.IsTrue(local.IsClose);
            }
        }
    }
}